/*

libualloc - microlib for memory allocation with various strategies

Copyright (c) 2020 Tibor 'Igor2' Palinkas
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the Author nor the names of contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

Source code: svn://svn.repo.hu/libualloc/trunk
Contact the author: http://igor2.repo.hu/contact.html

*/

#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include <libualloc/libualloc.h>

struct uall_acache_elem_s {
	uall_acache_elem_t *next;     /* ... in free_elems or used_elems */
	char data[1];                 /* real size is elem_size */
};

#define UALL_ACACHE_PTR2ELEM(ptr)       ((uall_acache_elem_t *)((char *)(ptr) - offsetof(uall_acache_elem_t, data)))
#define UALL_ACACHE_ELEM_T_SIZE         offsetof(uall_acache_elem_t, data)

UALL_INLINE void *uall_acache_alloc(uall_acache_t *ctx)
{
	uall_acache_elem_t *elem;

	if (UALL_OVERRIDE())
		return malloc(ctx->elem_size);

	if (ctx->free_elems != NULL) {
		elem = ctx->free_elems;
		ctx->free_elems = ctx->free_elems->next;
	}
	else
		elem = ctx->sys->alloc(ctx->sys, ctx->elem_size + UALL_ACACHE_ELEM_T_SIZE);

	return elem->data;
}

UALL_INLINE void uall_acache_free(uall_acache_t *ctx, void *ptr)
{
	uall_acache_elem_t *elem = UALL_ACACHE_PTR2ELEM(ptr);

	if (UALL_OVERRIDE()) {
		free(ptr);
		return;
	}

	/* link in the free list */
	elem->next = ctx->free_elems;
	ctx->free_elems = elem;
}

UALL_INLINE void uall_acache_flush(uall_acache_t *ctx)
{
	uall_acache_elem_t *elem, *next;

	for(elem = ctx->free_elems; elem != NULL; elem = next) {
		next = elem->next;
		ctx->sys->free(ctx->sys, elem);
	}
	ctx->free_elems = NULL;
}
