/*

libualloc - microlib for memory allocation with various strategies

Copyright (c) 2020 Tibor 'Igor2' Palinkas
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the Author nor the names of contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

Source code: svn://svn.repo.hu/libualloc/trunk
Contact the author: http://igor2.repo.hu/contact.html

*/

#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include <libualloc/libualloc.h>

struct uall_stackdnp_page_s {
	size_t alloced, used;
	uall_stackdnp_page_t *next;
	char data[1];
};

#define UALL_STACKDNP_PAGE_T_SIZE         offsetof(uall_stackdnp_page_t, data)

UALL_INLINE void *uall_stackdnp_alloc(uall_stackdnp_t *ctx, size_t size)
{
	uall_stackdnp_page_t *page = ctx->pages;
	size_t avail = (page == NULL) ? 0 : ctx->pages->alloced - ctx->pages->used;
	size_t totlen = UALL_ALIGN(size);
	void *elem;

	if (size == 0)
		return NULL;

	/* if not enough room in the current page, go on allocating the next page */
	if (avail < totlen) {
		size_t need = size + UALL_STACKDNP_PAGE_T_SIZE;
		size_t req = (need <= ctx->sys->page_size) ? ctx->sys->page_size : ((need / ctx->sys->page_size) + 1) * ctx->sys->page_size;
		page = ctx->sys->alloc(ctx->sys, req);
		if (page == NULL)
			return NULL;
		page->used = 0;
		page->alloced = req - UALL_STACKDNP_PAGE_T_SIZE;
		page->next = ctx->pages;
		ctx->pages = page;
	}

	/* current page has enough room, pack our data at the end */
	elem = ((char *)page->data + page->used);
	page->used += totlen;
	return elem;
}

UALL_INLINE void uall_stackdnp_clean(uall_stackdnp_t *ctx)
{
	uall_stackdnp_page_t *page, *next;
	for(page = ctx->pages; page != NULL; page = next) {
		next = page->next;
		ctx->sys->free(ctx->sys, page);
	}
	ctx->pages = NULL;
}
