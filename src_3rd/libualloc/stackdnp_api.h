#ifndef LIBUALLOC_STACKDNP_API_H
#define LIBUALLOC_STACKDNP_API_H

#include <stddef.h>
#include <libualloc/libualloc.h>

/*
	Allocator: stackdnp - stack allocation with dynamic sized data, no free/pop

	Allocate arbitrary sized elements in order, packing them in a linked list
	of pages. In other words, allocations are pushed on a stack. There is
	no free or pop operation, the whole stack needs to be discarded at once
	after use.

	Allocation size:      arbitrary
	Standard calls:       alloc, clean
	Per allocation cost:  0
	Per page cost:        1 pointers + 2 size_t + arbitrary fitting losses
*/

typedef struct uall_stackdnp_page_s uall_stackdnp_page_t;

typedef struct {
	/* configuration */
	uall_sysalloc_t *sys;

	void *user_data;

	/* internal states - init all bytes to 0 */
	uall_stackdnp_page_t *pages;
} uall_stackdnp_t;


/* Push data: allocate a block of size on top of the stack and return
   its pointer */
UALL_INLINE void *uall_stackdnp_alloc(uall_stackdnp_t *ctx, size_t size);

/* Free all data and empty ctx, which will be ready to accept new allocations;
   cheaper than calling uall_stackdnp_free() multiple times */
UALL_INLINE void uall_stackdnp_clean(uall_stackdnp_t *ctx);

#endif
