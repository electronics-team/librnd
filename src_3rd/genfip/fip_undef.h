
/* undef all instantiation macros so multiple instances can be included */

#undef FIP
#undef FIP_INT_SIZE
#undef FIP_FRAC_SIZE
#undef FIP_FUNC

#undef FIP_TYPE_NAME_
#undef FIP_TYPE_NAME
#undef FIP_MAX_
#undef FIP_MAX
#undef FIP_INT_MAX
#undef FIP_FRAC_MAX
