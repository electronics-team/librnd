/* fixed point integer minilib
   Copyright (C) 2016 Tibor 'Igor2' Palinkas
   This file is licensed under Creative Commons CC0 version 1.0, see
   https://creativecommons.org/publicdomain/zero/1.0/
   Source code: svn://svn.repo.hu/genfip/trunk
*/

#include <math.h>
#include <genfip/config.h>

#define FIP_TYPE_NAME_(width, signedness)   fip ## width ## signedness
#define FIP_TYPE_NAME(width, signedness)    FIP_TYPE_NAME_(width, signedness)

#define FIP_MAX_(x) x ## _MAX
#define FIP_MAX(x) FIP_MAX_(x)

#define FIP_INT_MAX   ((1ul << ((FIP_INT_SIZE*8ul)-1ul)) - 1ul)
#define FIP_FRAC_MAX  ((1ul << ((FIP_FRAC_SIZE*8ul))) - 1ul)

typedef struct FIP(_s) {
	signed i:FIP_INT_SIZE*8;
	unsigned f:FIP_FRAC_SIZE*8;
} FIP(_t);

#ifndef FIP_TYPES_ONLY

/* return a+b */
FIP_FUNC FIP(_t) FIP(_add)(FIP(_t) a, FIP(_t) b);

/* return a-b */
FIP_FUNC FIP(_t) FIP(_sub)(FIP(_t) a, FIP(_t) b);

/* return a*b */
FIP_FUNC FIP(_t) FIP(_mul)(FIP(_t) a, FIP(_t) b);

/* return -b */
FIP_FUNC FIP(_t) FIP(_neg)(FIP(_t) a);

/* return the double value of a */
FIP_FUNC double FIP(_to_double)(FIP(_t) a);

/* convert double a to a fip */
FIP_FUNC FIP(_t) FIP(_from_double)(double a);

#endif
