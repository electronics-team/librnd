#include <limits.h>

/* Largest integer is either long long (C89+extensions, C99) or long (C89) */
#ifdef LLONG_MAX
#	ifndef FIP_INT_MAX_VAL
#		define FIP_INT_MAX_VAL LLONG_MAX
#	endif
#	ifndef FIP_INT_MAX_TYPE
#		define FIP_INT_MAX_TYPE long long int
#	endif
#else
#	ifndef FIP_INT_MAX_VAL
#		define FIP_INT_MAX_VAL LONG_MAX
#	endif
#	ifndef FIP_INT_MAX_TYPE
#		define FIP_INT_MAX_TYPE long int
#	endif
#endif

/* Calculate the width (in bytes) of the largest native integer */
#ifndef FIP_INT_MAX_WIDTH
#	if FIP_INT_MAX_VAL == 127
#		define FIP_INT_MAX_WIDTH 1
#	elif FIP_INT_MAX_VAL == 32676
#		define FIP_INT_MAX_WIDTH 2
#	elif FIP_INT_MAX_VAL == 2147483647
#		define FIP_INT_MAX_WIDTH 4
#	elif FIP_INT_MAX_VAL == 9223372036854775807
#		define FIP_INT_MAX_WIDTH 8
#	endif
#endif


#ifndef FIP_FUNC
#	ifndef FIP_NO_INLINE
#		define FIP_FUNC static inline
#	else
#		define FIP_FUNC
#	endif
#endif
