#define FIP(name) fip11 ## name
#define FIP_INT_SIZE 1
#define FIP_FRAC_SIZE 1

#include <genfip/fip_implh.h>

#ifndef FIP_TYPES_ONLY
#include <genfip/fip_implc.h>
#endif

#include <genfip/fip_undef.h>
