/* fixed point integer minilib
   Copyright (C) 2016 Tibor 'Igor2' Palinkas
   This file is licensed under Creative Commons CC0 version 1.0, see
   https://creativecommons.org/publicdomain/zero/1.0/
   Source code: svn://svn.repo.hu/genfip/trunk
*/

#include <assert.h>
#include <string.h>

/*
#define FIP_MPI_WORD unsigned long int
#define FIP_MPI_IB (sizeof(FIP_MPI_WORD)/8)
*/

#define FIP_MPI_WORD unsigned char
#define FIP_MPI_IB (1)

#define fpi_mpi_words(size_in_bytes)  (((size_in_bytes-1)/FIP_MPI_IB)+1)

#define fip_mpi_sign_bit(src) ((*(unsigned char *)src) & 0x80)

#define fip_mpicpy(dst, dwlen, src, swlen) \
do { \
	FIP_MPI_WORD *__s__, *__d__, __fill__ = 0; \
	int __n__; \
	assert(swlen <= dwlen); \
	if (fip_mpi_sign_bit(src)) \
		__fill__--; \
	for(__n__ = 0, __d__ = dst; __n__ < dwlen - swlen; __n__++,__d__++) \
		*__d__ = __fill__; \
	for(__n__ = 0, __d__ = ((FIP_MPI_WORD *)dst) + (dwlen-swlen), __s__ = (FIP_MPI_WORD *)src; __n__ < swlen; __n__++,__d__++,__s__++) \
		*__d__ = *__s__; \
} while(0)

#define fip_mpi_import(dst, dwlen, stype, src) \
do { \
	FIP_MPI_WORD *__d__, __fill__ = 0; \
	unsigned char *__o__; \
	unsigned stype __tmp__; \
	int __n__; \
	assert(sizeof(stype) <= dwlen * sizeof(FIP_MPI_WORD)); \
	*(stype *)&__tmp__ = src; \
	if (src < 0) \
		__fill__--; \
	for(__n__ = 0, __d__ = dst; __n__ < dwlen; __n__++,__d__++) \
		*__d__ = __fill__; \
	__o__ = (unsigned char *)(dst + dwlen - 1); \
	while(__tmp__ > 0) { \
		*__o__ = __tmp__ & 0xFF; \
		__tmp__ >>= 8; \
		__o__--; \
	} \
} while(0)

#define fip_mpi_neg(num, wlen) \
do { \
	int __n__, __carry__; \
	FIP_MPI_WORD __max__ = 0; \
	__max__--; \
	num[wlen-1] ^= __max__; \
	__carry__ = (num[wlen-1] == __max__); \
	num[wlen-1]++; \
	for(__n__ = wlen-2; __n__ >= 0; __n__--) {\
		num[__n__] ^= __max__; \
		if (__carry__) { \
			__carry__ = (num[__n__] == __max__); \
			num[__n__]++; \
		} \
	} \
} while(0)

#define fip_mpi_to_double(dst, src, swlen) \
do { \
	double __mul__ = 1, __d__ = 0; \
	FIP_MPI_WORD __max__ = 0, __tmp__; \
	int __carry__, __n__, __isneg__ = fip_mpi_sign_bit(src); \
	__max__--; \
	__tmp__ = src[swlen-1]; \
	if (__isneg__) {\
		__tmp__ ^= __max__; \
		__carry__ = (__tmp__ == __max__); \
		__tmp__++; \
	}\
	__d__ = (double)__tmp__; \
\
	for(__n__ = swlen-2; __n__ >= 0; __n__--) { \
		FIP_MPI_WORD __tmp__ = src[__n__]; \
		if (__isneg__) {\
			__tmp__ ^= __max__; \
			if (__carry__) { \
				__carry__ = (__tmp__ == __max__); \
				__tmp__++; \
			} \
		} \
		__mul__ *= ((double)__max__ + 1.0); \
		__d__ += __mul__ * (double)__tmp__; \
	} \
	if (__isneg__) \
		dst = -(__d__); \
	else \
		dst = __d__; \
} while(0)


/* dst += num */
#define fip_mpi_add(dst, num, wlen) \
do { \
	int __n__, __carry__ = 0, __c__; \
	FIP_MPI_WORD __tmp__, __max__ = 0; \
	__max__--; \
	for(__n__ = wlen-1; __n__ >= 0; __n__--) {\
		__tmp__ = dst[__n__] + num[__n__]; \
		__c__ = (__tmp__ < dst[__n__]) || (__tmp__ < num[__n__]); \
		if (__carry__) { \
			__c__ = (__tmp__ == __max__); \
			__tmp__++; \
		} \
		dst[__n__] = __tmp__; \
		__carry__ = __c__; \
	} \
} while(0)

/* dst -= num */
#define fip_mpi_sub(dst, num, wlen) \
do { \
	int __n__, __carry__ = 0, __c__; \
	FIP_MPI_WORD __max__ = 0; \
	__max__--; \
	for(__n__ = wlen-1; __n__ >= 0; __n__--) {\
		__c__ = (num[__n__] > dst[__n__]); \
		dst[__n__] -= num[__n__]; \
		if (__carry__) { \
			__c__ |= (dst[__n__] == 0); \
			dst[__n__]--; \
		} \
		__carry__ = __c__; \
	} \
} while(0)

/* hi,lo = a*b */
#define fip_mpi_mul_11(hi,lo,a,b) \
do { \
	FIP_MPI_WORD __alo__, __ahi__, __blo__, __bhi__, __t0__, __t1__, __t2__; \
	FIP_MPI_WORD __mask_bits__ = sizeof(FIP_MPI_WORD)*8/2; \
	FIP_MPI_WORD __mask__ = ((FIP_MPI_WORD)1 << __mask_bits__) - 1; \
	__alo__ = (a) & __mask__; \
	__ahi__ = (a) >> __mask_bits__; \
	__blo__ = (b) & __mask__; \
	__bhi__ = (b) >> __mask_bits__; \
	__t0__ = __alo__*__blo__; \
	__t1__ = __ahi__*__blo__ + (__t0__ >> __mask_bits__); \
	__t2__ = __ahi__*__bhi__ + (__t1__ >> __mask_bits__); \
	__t1__ = __alo__*__bhi__ + (__t1__ & __mask__); \
	__t2__ += __t1__ >> __mask_bits__; \
	lo = (__t0__ & __mask__) + (__t1__ << __mask_bits__); \
	hi = __t2__; \
} while(0)

/*
 hi,lo = a*b+c 
#define fip_mpi_muladd_111(hi,lo,a,b,c) \
do { \
	fip_mpi_mul_11(hi, lo, a, b); \
	lo += (c); \
	hi += lo < (c); \
} while(0)
*/

/*
 carry,r = a+b+carry 
#define fip_mpi_add_11(carry,r,a,b) \
do { \
	FIP_MPI_WORD __a__=a, __b__=b; \
	FIP_MPI_WORD __t1__, __t2__; \
	__t1__ = __a__ + carry; \
	__t2__ = __b__ + __t1__; \
	carry = __t1__ < __a__ || __t2__ < __b__; \
	r = __t2__; \
} while(0)
*/

/*
// hi,a[] = b[]*c + r
#define fip_mpi_muladd_n11(hi,a,b,wlen,c,r) \
do { \
	int __i__; \
	for (__i__ = wlen-1; __i__ >= 0; __i__--) { \
		fip_mpi_mul_11(hi, a[__i__], b[__i__], c); \
		a[__i__] += r; \
		hi += a[__i__] < r; \
		r = hi; \
	} \
} while(0)
*/

/* hi,a[0..alen-1] += b[0..blen-1]*c */
#define fip_mpi_addmul_n1(hi,a,alen,b,blen,c) \
do { \
	FIP_MPI_WORD __t0__, __t1__; \
	int __i__; \
	hi = 0; \
	for (__i__ = 0; __i__ < alen; __i__++) { \
		__t0__ = a[alen-1-__i__] + hi; \
		__t1__ = __t0__ < hi; \
		fip_mpi_mul_11(hi, a[alen-1-__i__], b[blen-1-__i__], c); \
		a[alen-1-__i__] += __t0__; \
		hi += a[alen-1-__i__] < __t0__; \
		hi += __t1__; \
	} \
} while(0)


#define fip_mpi_zero(a,wlen) memset(a, 0, (wlen) * sizeof*(a))

/*
 a != b, a != c
 a[] = b[]*c[]
*/
#define fip_mpi_mul_assign(a,b,c,wlen) \
do { \
	int __j__; \
	FIP_MPI_WORD __hi__; \
	fip_mpi_zero(a, wlen); \
	for (__j__ = wlen-1; __j__ >= 0; __j__--) { \
		if (c[__j__]) \
			fip_mpi_addmul_n1(__hi__, a, __j__+1, b, wlen, c[__j__]); \
	} \
} while(0)

/* dst *= num */
#define fip_mpi_mul(dst, num, wlen) \
do { \
	FIP_MPI_WORD __t__[wlen]; \
	fip_mpi_mul_assign(__t__, dst, num, wlen); \
	memcpy(dst, __t__, wlen); \
} while(0)
