#include "genprints.h"

typedef struct vect_s {
	int x, y;
} vect_t;


static long Pprint_vect(const void *val, Pfmt_t *format, void (*put_ch)(void *pctx, int ch), void *pctx)
{
	char tmp[128];
	int len;
	vect_t *v = *((vect_t **)val);

	len = sprintf(tmp, "[%d;%d]", v->x, v->y);

	/* note: vectors don't support formatting */
	P_PRINT_STR(tmp);

	return len;
}

#define Pvect(val)         PMAGIC, PTYPE(' ', 'p'), Pprint_vect, NULL, (vect_t *)(&(val))


int main()
{
	vect_t v = {42, 111};
	void *p = 0x4000;
	char buff[128];
	long len;

	prints(
		"I      |", PintF(-23, 9, PFF_PAD('`')), "\n",
		"I      |", PintF(+23, 9, PFF_PAD('`')), "\n",
		"I left |", PintF(-23, 9, PFF_PAD('`') | PFF_ARIGHT), "\n",
		"I cent |", PintF(-23, 9, PFF_PAD('`') | PFF_ACENT), "\n",
		0);

	prints(
		"S      |", Pstring("hello"), "\n",
		"S      |", PstringF("hello", 12, PFF_PAD('`')), "\n",
		"S left |", PstringF("hello", 12, PFF_PAD('`') | PFF_ARIGHT), "\n",
		"S cent |", PstringF("hello", 12, PFF_PAD('`') | PFF_ACENT), "\n",
		0);

	prints(
		"S0     |", Pstring(NULL), "\n",
		0);

	prints(
		"V1     |", Pvect(v), "\n",
		0);

	prints(
		"P0     |", Ppointer(NULL), "\n",
		"P1     |", Ppointer(p), "\n",
		0);

	len = snprints(buff, 4, Pint(1), 0);
	printf("sn1 '%s' %ld\n", buff, len);

	len = snprints(buff, 4, Pint(12), 0);
	printf("sn2 '%s' %ld\n", buff, len);

	len = snprints(buff, 4, Pint(123), 0);
	printf("sn3 '%s' %ld\n", buff, len);

	len = snprints(buff, 4, Pint(1234), 0);
	printf("sn4 '%s' %ld\n", buff, len);

	len = snprints(buff, 4, Pint(123456789), 0);
	printf("sn9 '%s' %ld\n", buff, len);

	prints(
		"C0     |", Pchar('q'), "\n",
		"C1     |", PcharF('q', 3, PFF_PAD('`')), "\n",
		0);

	prints(
		"DF0    |", PdblF(1234.12345678901234567890, 0,4,  PFF_PAD('`')), "\n",
		"DF1    |", PdblF(1234.12345678901234567890, 3,9,  PFF_PAD('`')), "\n",
		"DF2    |", PdblF(+1234.12345678901234567890, 4,12, PFF_PAD('`') | PFF_ARIGHT), "\n",
		"DF3    |", PdblF(-1234.12345678901234567890, 4,12, PFF_PAD('`') | PFF_ARIGHT), "\n",
		"DF4    |", PdblF(+1234.12345678901234567890, 8,12, PFF_PAD('`') | PFF_ALEFT), "\n",
		"DF5    |", PdblF(-1234.12345678901234567890, 8,12, PFF_PAD('`') | PFF_ALEFT), "\n",
		"DF6    |", PdblF(+1234.12345678901234567890, 8,12, PFF_PAD('`') | PFF_ACENT), "\n",
		"DF7    |", PdblF(-1234.12345678901234567890, 8,12, PFF_PAD('`') | PFF_ACENT), "\n",
		"DF8    |", PdblF(-1234.1, 8,12, PFF_PAD('`') | PFF_ACENT), "\n",
		"DF9    |", PdblF(-1234.1, 8,12, PFF_PAD('`') | PFF_ACENT | PFF_NO_T0), "\n",
		0);

	return 0;
}
