/*
    puplug - portable micro plugin framework
    Copyright (C) 2017 Tibor 'Igor2' Palinkas

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "puplug.h"
#include "error.h"

int pup_debug_uninit = 0;

#define Fprintf     if (pup_debug_uninit) fprintf

void pup_init(pup_context_t *pup)
{
	memset(pup, 0, sizeof(pup_context_t));
}

static long pup_uninit_soft(pup_context_t *pup)
{
	pup_plugin_t *n, *next;
	long cnt = 0;

	/* unload all top-level (app-loaded) libs */
	for(n = pup->plugins; n != NULL; n = next) {
		next = n->next;
		if (n->flags & PUP_FLG_TOPLEVEL) {
			Fprintf(stderr, " unld %s %d %d\n", n->name, n->references, n->app_refco);
			pup_unload(pup, n, NULL);
			cnt++;
			next = pup->plugins; /* next is unsafe, it may have been free'd */
		}
	}

	return cnt;
}

/* unload all libs that have only one ref */
static long pup_uninit_hard(pup_context_t *pup)
{
	pup_plugin_t *n, *next;
	long cnt = 0;

	/* unload all top-level loaded libs */
	for(n = pup->plugins; n != NULL; n = next) {
		next = n->next;

		if (n->references == 1) {
			Fprintf(stderr, " unld %s %d %d\n", n->name, n->references, n->app_refco);
			pup_unload_(pup, n, NULL);
			cnt++;
			next = pup->plugins; /* next is unsafe, it may have been free'd */
		}
	}

	return cnt;
}

void pup_uninit_list(pup_context_t *pup)
{
	pup_plugin_t *n;

	fprintf(stderr, "List {\n");
	for(n = pup->plugins; n != NULL; n = n->next)
		fprintf(stderr, " %s %d top=%d %d\n", n->name, n->references, (n->flags & PUP_FLG_TOPLEVEL), n->app_refco);
	fprintf(stderr, "}\n");
}

void pup_uninit(pup_context_t *pup)
{

	/* first try to unload only those that are still loaded by the app; this is
	   the so called soft unload. Repeat as long as we unload plugins this way.
	   What's left is a bunch of auto-loaded plugins at the end, but normally
	   nothing is left. */
	Fprintf(stderr, "pup_uninit soft ----- \n");
	for(;;) {
		if (pup_debug_uninit) pup_uninit_list(pup);
		if (pup_uninit_soft(pup) == 0)
			break;
	}

	if (pup->plugins != NULL)
		fprintf(stderr, "pup_uninit(): warning: some plugins are not soft-unloaded but not marked as app-loaded either\n");

	/* The rest are auto-loaded plugins that somehow did not get unloaded via
	   deps. This is not normal at all. Still: try to unload them relatively
	   gracefully */
	Fprintf(stderr, "pup_uninit hard - \n");
	for(;;) {
		if (pup_debug_uninit) pup_uninit_list(pup);
		if (pup_uninit_hard(pup) == 0)
			break;
	}

	Fprintf(stderr, "uninit END  ----- %p \n", pup->plugins);
	if (pup->plugins != NULL)
		fprintf(stderr, "pup_uninit(): error: some plugins are not unloaded; potential cyclic dependency\n");
	pup_err_stack_destroy(pup, pup->err_stack);
	free(pup->bu);
}
